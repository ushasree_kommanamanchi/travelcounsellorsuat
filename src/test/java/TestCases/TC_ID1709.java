package TestCases;

import java.io.IOException;
import org.testng.annotations.Test;
import Components.Scenario_ID1709;
import Utilities.BaseClass;

public class TC_ID1709 extends BaseClass {

	@Test
	public void FAQ() throws InterruptedException, IOException {
		Scenario_ID1709 SC_ID1709 = new Scenario_ID1709(driver);
		SC_ID1709.GetQuoteTest();
		SC_ID1709.DocumentsTest();

	}
}
