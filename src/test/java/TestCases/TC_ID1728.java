package TestCases;

import java.io.IOException;
import org.testng.annotations.Test;
import Components.Scenario_ID1728;
import Utilities.BaseClass;

public class TC_ID1728 extends BaseClass {

	@Test
	public void Products() throws InterruptedException, IOException {
		Scenario_ID1728 SC_ID1728 = new Scenario_ID1728(driver);
		SC_ID1728.GetQuoteTest();
		SC_ID1728.PreExistingMedicalQuesTest();
		SC_ID1728.QuotePageTest();

	}
}
