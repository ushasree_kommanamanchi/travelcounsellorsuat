package TestCases;

import java.io.IOException;

import org.testng.annotations.Test;

import Components.Scenario_ID1693;
import Utilities.BaseClass;

public class TC_ID1693 extends BaseClass {

	@Test
	public void LiveChat() throws InterruptedException, IOException {
		Scenario_ID1693 SC_ID1693 = new Scenario_ID1693(driver);
		SC_ID1693.GetQuoteTest();
		SC_ID1693.LiveChatTest();

	}
}
