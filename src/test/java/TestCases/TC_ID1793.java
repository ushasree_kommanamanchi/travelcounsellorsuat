package TestCases;

import java.io.IOException;

import org.testng.annotations.Test;
import Components.Scenario_ID1793;
import Utilities.BaseClass;

public class TC_ID1793 extends BaseClass {
	
	@Test
	public void Endoresements() throws InterruptedException, IOException {
		Scenario_ID1793 SC_ID1793 = new Scenario_ID1793(driver);
		SC_ID1793.GetQuoteTest();
		SC_ID1793.PreExistingMedicalQuesTest();
		SC_ID1793.QuotePageTest();
	}

}
