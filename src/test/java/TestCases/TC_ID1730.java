package TestCases;

import java.io.IOException;
import org.testng.annotations.Test;
import Components.Scenario_ID1730;
import Utilities.BaseClass;

public class TC_ID1730 extends BaseClass {
	
	@Test
	public void Cookies() throws InterruptedException, IOException {
		Scenario_ID1730 SC_ID1730 = new Scenario_ID1730(driver);
		SC_ID1730.GetQuoteTest();
		SC_ID1730.DocumentsTest();
	
	}

}
