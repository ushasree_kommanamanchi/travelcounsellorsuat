package Components;

import java.io.IOException;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import PageObjects.TC_WebElements;
import Utilities.BaseClass;

public class Scenario_ID1817 extends BaseClass {
	
	public Scenario_ID1817(WebDriver driver) {
		this.driver = driver;
	}

	@Test
	public void GetQuoteTest(String T1DOB, String T2DOB, String T3DOB, String T4DOB, String T5DOB, String T6DOB, String T7DOB, String T8DOB, String T9DOB, String T10DOB) throws InterruptedException, IOException {
		TC_WebElements TC_obj = new TC_WebElements(driver);
		// Thread.sleep(5000);
		TC_obj.acceptCookies.click();
		logger.info("Accepted cookies");
		TC_obj.SingleTrip.click();
		logger.info("Clicked Single Trip");
		TC_obj.Europe.click();
		TC_obj.Today.click();
		TC_obj.fiveNights.click();
		TC_obj.Group.click();
		TC_obj.Groupten.click();
		TC_obj.T1DOB.sendKeys(T1DOB);
		TC_obj.T1DOB.sendKeys(Keys.TAB);
		TC_obj.T2DOB.sendKeys(T2DOB);
		Thread.sleep(2000);
		TC_obj.T2DOB.sendKeys(Keys.TAB);
		TC_obj.T3DOB.sendKeys(T3DOB);
		TC_obj.T3DOB.sendKeys(Keys.TAB);
		TC_obj.T4DOB.sendKeys(T4DOB);
		TC_obj.T4DOB.sendKeys(Keys.TAB);
		Thread.sleep(2000);
		TC_obj.T5DOB.sendKeys(T5DOB);
		TC_obj.T5DOB.sendKeys(Keys.TAB);
		TC_obj.T6DOB.sendKeys(T6DOB);
		TC_obj.T6DOB.sendKeys(Keys.TAB);
		Thread.sleep(2000);
		TC_obj.T7DOB.sendKeys(T7DOB);
		TC_obj.T7DOB.sendKeys(Keys.TAB);
		TC_obj.T8DOB.sendKeys(T8DOB);
		TC_obj.T8DOB.sendKeys(Keys.TAB);
		Thread.sleep(2000);
		TC_obj.T9DOB.sendKeys(T9DOB);
		TC_obj.T9DOB.sendKeys(Keys.TAB);
		TC_obj.T10DOB.sendKeys(T10DOB);
		TC_obj.T10DOB.sendKeys(Keys.TAB);
		Thread.sleep(2000);
		logger.info(
				"Scenario: As a customer I want to see the GDPR statement");
		TC_obj.Continue.click();
		logger.info("Clicked on continue and redirected to Pre existing Medical Questions");

	}

}
