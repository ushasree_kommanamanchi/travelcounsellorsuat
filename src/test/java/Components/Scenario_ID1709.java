package Components;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import PageObjects.TC_WebElements;
import Utilities.BaseClass;

public class Scenario_ID1709 extends BaseClass  {

	public Scenario_ID1709(WebDriver driver) {
		this.driver = driver;
	}


	public void GetQuoteTest() throws InterruptedException, IOException {
		TC_WebElements TC_obj = new TC_WebElements(driver);
		//Thread.sleep(5000);
		TC_obj.acceptCookies.click();
		logger.info("Accepted cookies");
	
}
	

	public void DocumentsTest() throws InterruptedException {
		TC_WebElements TC_obj = new TC_WebElements(driver);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		TC_obj.footerFAQ.click();
		
		//doc.faqHeading();
		
		WebDriverWait wait = new WebDriverWait(driver, 70);
		WebElement Livechat = wait
				.until(ExpectedConditions.visibilityOf(TC_obj.Livechat));
	  Assert.assertEquals(TC_obj.Livechat.getText(),
	  "Live Chat");
	}
}
