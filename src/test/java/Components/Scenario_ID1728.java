package Components;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;

import PageObjects.TC_WebElements;
import Utilities.BaseClass;

public class Scenario_ID1728 extends BaseClass {
	
	public Scenario_ID1728(WebDriver driver) {
		this.driver = driver;
	}


	public void GetQuoteTest() throws InterruptedException, IOException {
		TC_WebElements TC_obj = new TC_WebElements(driver);
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		TC_obj.acceptCookies.click();
		logger.info("Accepted cookies");
		TC_obj.SingleTrip.click();
		logger.info("Clicked Single Trip");
		TC_obj.Europe.click();
		TC_obj.Today.click();
		TC_obj.fiveNights.click();
		TC_obj.OnePerson.click();
		TC_obj.Calendar.click();
		TC_obj.SelectMonth.click();
		TC_obj.Month.click();
		TC_obj.SelectYear.click();
		TC_obj.Year.click();
		TC_obj.SelectDate.click();
		logger.info(
				"Scenario: Selected Single Trip, Europe, Today, five nights, Individual, selected date of birth 1990, Travel Only, Premier Product with Business Cover");
		TC_obj.Continue.click();
		logger.info("Clicked on continue and redirected to Pre existing Medical Questions");

	}


	public void PreExistingMedicalQuesTest() throws InterruptedException {

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);

		TC_WebElements TC_obj = new TC_WebElements(driver);
		TC_obj.illness_question.click();
		TC_obj.symptoms_question.click();
		TC_obj.conditions_question.click();
		logger.info("Answered Traveler Journey Questions");
		TC_obj.ClickQuotePage.click();
		logger.info("Clicked on continue and will navigate to Quote Page");
		Thread.sleep(4000);

	}


	public void QuotePageTest() throws InterruptedException {

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);

		TC_WebElements TC_obj = new TC_WebElements(driver);
		TC_obj.Classic.click();
		logger.info("Clicked on Classic Product");
	}

}
