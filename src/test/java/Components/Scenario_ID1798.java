package Components;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import PageObjects.TC_WebElements;
import Utilities.BaseClass;

public class Scenario_ID1798 extends BaseClass {

	public Scenario_ID1798(WebDriver driver) {
		this.driver = driver;
	}

	@Test
	public void GetQuoteTest() throws InterruptedException, IOException {
		driver.get(baseURL);

		logger.info("URL is opened");
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);

		TC_WebElements TC_obj = new TC_WebElements(driver);
		TC_obj.acceptCookies.click();
		logger.info("Accepted cookies");
		TC_obj.SingleTrip.click();
		logger.info("Clicked Single Trip");
		TC_obj.Europe.click();
		TC_obj.Today.click();
		TC_obj.fiveNights.click();
		TC_obj.OnePerson.click();
		TC_obj.Calendar.click();
		TC_obj.SelectMonth.click();
		TC_obj.Month.click();
		TC_obj.SelectYear.click();
		TC_obj.Year.click();
		TC_obj.SelectDate.click();
		TC_obj.WSCover.click();
		TC_obj.CruiseCover.click();
		Thread.sleep(3000);
		TC_obj.GolfCover.click();
		Thread.sleep(3000);
		TC_obj.WSCover.click();
		Thread.sleep(3000);
		logger.info("Scenario: A user can add policy addons to his order in getquote Page");
		TC_obj.Continue.click();
		logger.info("Clicked on continue and redirected to Pre existing Medical Questions");

	}


}
