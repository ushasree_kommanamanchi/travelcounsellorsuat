package Components;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import PageObjects.TC_WebElements;
import Utilities.BaseClass;

public class Scenario_ID1793 extends BaseClass {

	public Scenario_ID1793(WebDriver driver) {
		this.driver = driver;
	}

	@Test
	public void GetQuoteTest() throws InterruptedException, IOException {
		TC_WebElements TC_obj = new TC_WebElements(driver);
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		TC_obj.acceptCookies.click();
		logger.info("Accepted cookies");
		TC_obj.SingleTrip.click();
		logger.info("Clicked Single Trip");
		TC_obj.Europe.click();
		TC_obj.Today.click();
		TC_obj.fiveNights.click();
		TC_obj.OnePerson.click();
		TC_obj.Calendar.click();
		TC_obj.SelectMonth.click();
		TC_obj.Month.click();
		TC_obj.SelectYear.click();
		TC_obj.Year.click();
		TC_obj.SelectDate.click();
		logger.info(
				"Scenario: A user can add further cover (Endorsments)");
		TC_obj.Continue.click();
		logger.info("Clicked on continue and redirected to Pre existing Medical Questions");
	}

	@Test
	public void PreExistingMedicalQuesTest() throws InterruptedException {

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);

		TC_WebElements TC_obj = new TC_WebElements(driver);
		TC_obj.illness_question.click();
		TC_obj.symptoms_question.click();
		TC_obj.conditions_question.click();
		logger.info("Answered Traveler Journey Questions");
		TC_obj.ClickQuotePage.click();
		logger.info("Clicked on continue and will navigate to Quote Page");
		Thread.sleep(4000);
	}

	@Test
	public void QuotePageTest() throws InterruptedException {

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);

		TC_WebElements TC_obj = new TC_WebElements(driver);
		TC_obj.Premier.click();
		logger.info("Clicked on Premier Product");
		TC_obj.BusinessCover.click();
		logger.info("Clicked on Business Cover");
		Thread.sleep(3000);
		TC_obj.Flight_Cancellation.click();
		logger.info("Added flight cancellation");
		Thread.sleep(2000);
		TC_obj.Flight_Cancellation_Remove.click();
		logger.info("Removed flight cancellation");
		Thread.sleep(3000);

		TC_obj.clickedContinue.click();
		logger.info("Clicked on continue and will navigate to Traveler Details Page");

	}

}
